const express = require("express");
const fs = require("fs");
const HmacSHA256 = require("crypto-js/hmac-sha256");
const base64 = require("crypto-js/enc-base64");

const app = express();
const port = 3000;
const secret = "secretkeydaa";

app.use(express.json());

app.listen(port, () => {
  console.log("server is running");
});

app.get("/", (req, res) => {
  res.send("Hello World!");
});

function RequestObject(reqFile, reqHmac, serHmac, valid) {
  this.reqFile = reqFile;
  this.reqHmac = reqHmac;
  this.serHmac = serHmac;
  this.valid = valid;
}

function writeToDirectory(data, fileName) {
  fs.writeFile(
    `./serverTest/${fileName}.txt`,
    data,
    { encoding: "base64" },
    function (err) {}
  );
}

app.post("/sendfile", async (req, res) => {
  const { data } = req.body;
  let logData = [];

  for (let index = 0; index < data.length; index++) {
    const ele = data[index];
    const sha256 = HmacSHA256(ele.file, secret);
    const serverHmac = base64.stringify(sha256);
    const isValid = serverHmac === ele.hmac ? true : false;
    writeToDirectory(ele.file, ele.fileName);
    logData[index] = new RequestObject(ele.file, ele.hmac, serverHmac, isValid);
  }
  console.table(logData);
});
