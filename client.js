const axios = require("axios");
const fs = require("fs");
const HmacSHA256 = require("crypto-js/hmac-sha256");
const base64 = require("crypto-js/enc-base64");

const api = axios.create({
  baseURL: "http://localhost:3000",
});

const secret = "secretkeydaa";

const testCase = [
  { name: "test1", status: "valid" },
  { name: "test2", status: "valid" },
  { name: "test3", status: "valid" },
  { name: "test4", status: "valid" },
  { name: "test5", status: "valid" },
  { name: "test6", status: "corrupt" },
  { name: "test7", status: "corrupt" },
  { name: "test8", status: "corrupt" },
  { name: "test9", status: "corrupt" },
  { name: "test10", status: "corrupt" },
];

function RequestLog(file, hmac) {
  this.file = file;
  this.hmac = hmac;
}

const request = async (data) => {
  return await api.post("/sendfile", data).then((res) => console.log(res.data));
};

const getCorruptFile = (fileName) => {
  return fs.readFileSync(`./clientTest/${fileName}Corrupt.txt`, {
    encoding: "base64",
  });
};

const test = () => {
  let logData = [];
  let reqData = [];

  for (let index = 0; index < testCase.length; index++) {
    const obj = testCase[index];
    let corruptFile = "";

    if (obj.status !== "valid") {
      corruptFile = getCorruptFile(obj.name);
    }

    file = fs.readFileSync(`./clientTest/${obj.name}.txt`, {
      encoding: "base64",
    });

    const sha256 = HmacSHA256(file, secret);
    const clientHmac = base64.stringify(sha256);
    logData[index] = new RequestLog(file, clientHmac);
    const isCorrupted = corruptFile !== "" ? corruptFile : file;
    reqData[index] = {
      file: isCorrupted,
      hmac: clientHmac,
      fileName: obj.name,
    };
  }

  console.table(logData);

  request({
    data: reqData,
  });
};

test();
